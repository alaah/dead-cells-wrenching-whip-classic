![screenshot](https://steamuserimages-a.akamaihd.net/ugc/924811883707504933/DFD6894884578009F249EC1B85130C90C688AEB9/)

# Howto
This is suited for [barebones](https://gitlab.com/alaah/dead-cells-barebones).

# License
See LICENSE for information.

Of course this is based on absolutely proprietary files. I consider my "changes" to be GPL. No one will care anyway.